<?php

/**
 * @file
 * uc_dineromail menu items.
 */

/**
 * Return page for DineroMail failure response.
 */
function uc_dineromail_failure($order_id = 0) {
  return t('An error has occurred during payment. Please contact us to ensure your order has been submitted.');
}

/**
 * Return page for DineroMail success response.
 */
function uc_dineromail_success($order_id = 0) {
  $order = uc_order_load($order_id);
  // If the order ID specified in the return URL is not the same as the one in
  // the user's session, we need to assume this is either a spoof or that the
  // user tried to adjust the order on this side while at DineroMail. If it was
  // a legitimate checkout, the IPN will still come in from DineroMail so the
  // order gets processed correctly. We'll leave an ambiguous message just in
  // case.
  if (intval($_SESSION['cart_order']) != $order->order_id) {
    drupal_set_message(t('Thank you for your order! We will be notified by DineroMail that we have received your payment.'));
    drupal_goto('cart');
  }

  // Ensure the payment method is DineroMail.
  if ($order->payment_method != 'dineromail') {
    drupal_goto('cart');
  }

  // This lets us know it's a legitimate access of the complete page.
  $_SESSION['uc_checkout'][$order_id]['do_complete'] = TRUE;
  drupal_goto('cart/checkout/complete');
}

/**
 * IPN handler for DineroMail.
 *
 * This function is responsible for receiving and processing the notifications
 * sent from DineroMail IPN.
 */
function uc_dineromail_ipn() {
  module_load_include('inc', 'uc_dineromail', 'uc_dineromail.ipn');
  watchdog('uc_dineromail', 'DineroMail IPN notification received', array(), WATCHDOG_NOTICE);
  if (!isset($_POST['Notificacion'])) {
    watchdog('uc_dineromail', 'Invalid IPN attempt', array(), WATCHDOG_ERROR);
    return;
  }
  watchdog('uc_dineromail', 'IPN notification received is:<br />!notification', array('!notification' => check_plain($_POST['Notificacion'])), WATCHDOG_NOTICE);

  $notification = _uc_dineromail_load_xml($_POST['Notificacion']);
  // Malformed xml in request.
  if (!$notification) {
    return;
  }

  $operations = _uc_dineromail_get_operations($notification);
  if (empty($operations['operations'])) {
    watchdog('uc_dineromail', 'IPN notification contained no operations:<br />!notification', array('!notification' => check_plain($_POST['Notificacion'])), WATCHDOG_ERROR);
    return;
  }

  foreach ($operations['operations'] as $op) {
    _uc_dineromail_log_ipn($op, 'Notification');
  }

  $response = _uc_dineromail_send_query($operations);
  if ($response) {
    _uc_dineromail_process_response($response);
  }
}
