<?php

/**
 * @file
 * uc_dineromail IPN related functions.
 *
 * DineroMail IPN calls our url with an array of operations that has changed in
 * the system and we need to ask for the state of that operations to udpate our
 * orders.
 */

/**
 * Translates xml to object. Returns false in case of error.
 *
 * @param string $xml_str
 *   XML string
 *
 * @return SimpleXMLElement
 *   SimpleXMLElement object representing the received xml or FALSE in case of
 *   error.
 */
function _uc_dineromail_load_xml($xml_str) {
  libxml_use_internal_errors(TRUE);
  try{
    return new SimpleXMLElement($xml_str);
  } catch (Exception $e){
    watchdog('uc_dineromail', 'Invalid XML from IPN: !msg<br />!xml', array('!msg' => $e->getMessage(), '!xml' => check_plain($xml_str)), WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 * Extracts the operations contained in the notification sent by DineroMail.
 *
 * @param SimpleXMLElement $notification
 *   Notification received from DineroMail IPN.
 *
 * @return array
 *   An array containing an array with the operation ids included in the
 *   notification ('operations'), and the type of the notification ('type').
 */
function _uc_dineromail_get_operations($notification) {
  // TODO: currently there is only one possible type in IPN documentation, but
  // we should probably take this from the $notification.
  $type = '1';
  $operations = array();
  if ($notification->operaciones->operacion) {
    foreach ($notification->operaciones->operacion as $operation) {
      $operations[] = $operation->id;
    }
  }
  return array(
    'operations' => $operations,
    'type' => $type,
  );
}

/**
 * Sends the request to DineroMail for the operation ids in $operations.
 *
 * @param array $operations
 *   Array contanining the operations and notification type to send to
 *   DineroMail, as returned by _uc_dineromail_get_operations.
 *
 * @return stdClass
 *   The request response as returned by drupal_http_request.
 */
function _uc_dineromail_send_query($operations) {
  // Prepare query xml.
  $report = new simpleXMLElement('<REPORTE></REPORTE>');
  $report->addChild('NROCTA', variable_get('uc_dineromail_account', ''));
  $detalle = $report->addChild('DETALLE');
  $consulta = $detalle->addChild('CONSULTA');
  $consulta->addChild('CLAVE', variable_get('uc_dineromail_ipn_key', ''));
  $consulta->addChild('TIPO', $operations['type']);
  $ops = $consulta->addChild('OPERACIONES');
  foreach ($operations['operations'] as $o) {
    $ops->addChild('ID', $o);
  }
  // Prepare request.
  $host = _uc_dineromail_get_ipn_url();
  if (!$host) {
    watchdog('uc_dineromail', 'Unable to get IPN URL for the request.', array(), WATCHDOG_ERROR);
    return FALSE;
  }

  $data = 'DATA=' . _uc_dineromail_as_xml($report);
  $response = drupal_http_request($host, array('Content-Type' => 'application/x-www-form-urlencoded'), 'POST', $data);

  if (array_key_exists('error', $response)) {
    watchdog('uc_dineromail', 'IPN query for order ids: @order_ids failed with HTTP error @error, code @code.',
      array(
        '@error' => $response->error,
        '@code' => $response->code,
        '@order_ids' => implode(',', $operations['operations']),
      ), WATCHDOG_ERROR);
    return FALSE;
  }
  watchdog('uc_dineromail', 'IPN query for order ids: @order_ids OK. Response was:<br />@response',
    array(
      '@order_ids' => implode(',', $operations['operations']),
      '@response' => $response->data),
    WATCHDOG_NOTICE);
  return $response;
}

/**
 * Process the response XML sent by DineroMail.
 */
function _uc_dineromail_process_response($response) {
  $data = _uc_dineromail_load_xml($response->data);
  if (!$data) {
    watchdog('uc_dineromail', 'Invalid response from DineroMail', array(), WATCHDOG_ERROR);
    return FALSE;
  }

  if (_uc_dineromail_check_report_status($data->ESTADOREPORTE)) {
    $operations = $data->DETALLE->OPERACIONES;
  }

  $context = array(
    'revision' => 'formatted-original',
    'type' => 'amount',
  );

  if (!$operations) {
    return;
  }
  foreach ($operations->children() as $op) {
    $order_id = strval($op->ID);
    $payment_amount = strval($op->MONTO);
    $txn_id = strval($op->NUMTRANSACCION);
    $email = strval($op->EMAIL);
    $payment_method = strval($op->MEDIOPAGO);
    $payment_status = _uc_dineromail_get_status(strval($op->ESTADO));

    $duplicate = db_result(db_query("SELECT COUNT(*) FROM {uc_payment_dineromail_ipn} WHERE order_id = %d AND status NOT IN ('Pending', 'Notification')", $order_id));
    if ($duplicate > 0) {
      watchdog('uc_dineromail', 'IPN transaction ID @id has been processed before.', array('@id' => $order_id), WATCHDOG_NOTICE);
      return;
    }

    switch ($op->ESTADO) {
      // PENDIENTE DE PAGO.
      case '1':
        uc_order_update_status($order_id, 'dineromail_pending');
        uc_order_comment_save($order_id, 0, t('DineroMail IPN reported transaction !trx as pending.', array('!trx' => $op->NUMTRANSACCION)));
        break;

      // ACREDITADO.
      case '2':
        $order = uc_order_load($order_id);
        $comment = t('DineroMail transaction ID: @txn_id', array('@txn_id' => $txn_id));
        uc_payment_enter($order_id, 'dineromail', $payment_amount, 0, NULL, $comment);
        uc_cart_complete_sale($order);
        uc_order_comment_save($order_id, 0, t('Payment of @amount submitted through DineroMail.', array('@amount' => '$' . $payment_amount)), 'order', 'payment_received');
        uc_order_comment_save($order_id, 0, t('DineroMail IPN reported a payment of @amount made with @payment_method', array('@amount' => '$' . $payment_amount, '@payment_method' => $payment_method)));
        break;

      // CANCELADO.
      case '3':
        uc_order_comment_save($order_id, 0, t("Payment cancelled in DineroMail: order id !order.", array('!order' => $order_id)), 'admin');
        break;
    }
    _uc_dineromail_log_ipn($order_id, $payment_status, $txn_id, $payment_method, $payment_amount, $payer_email);
  }
}

/**
 * Checks the status code returned by DineroMail.
 *
 * Returns TRUE if status is 1 and FALSE otherwise.
 */
function _uc_dineromail_check_report_status($status) {
  $status = strval($status);
  if ($status == '1') {
    return TRUE;
  }
  $messages = array(
    '2' => t('Malformed XML'),
    '3' => t('Invalid account number (syntax)'),
    '4' => t('Invalid password (syntax)'),
    '5' => t('Invalid query type'),
    '6' => t('Invalid operation ID'),
    '7' => t('Invalid account or password'),
    '8' => t('No operations to report'),
  );
  $error = ($messages[$status]) ? $messages[$status] : t('Unknown error');
  watchdog('uc_dineromail', 'DineroMail IPN response error code !code: !error', array('!code' => $status, '!error' => $error), WATCHDOG_WARNING);
  return FALSE;
}

/**
 * Returns an xml string representation without the header tag.
 *
 * DineroMail complains if we include this tag.
 */
function _uc_dineromail_as_xml($xml) {
  $temp = explode("\n", $xml->asXML());
  unset($temp[0]);
  return implode('', $temp);
}

/**
 * Returns status string.
 *
 * @param string $status
 *   status code from DineroMail.
 */
function _uc_dineromail_get_status($status) {
  $states = array(
    '1' => 'Pending',
    '2' => 'Accredited',
    '3' => 'Cancelled',
  );
  return $states[$status];
}

/**
 * Returns the name of the payment method informed by DineroMail IPN.
 *
 * @param string $method
 *   method code from DineroMail.
 */
function _uc_dineromail_get_method($method) {
  $country = variable_get('uc_dineromail_country', '');

  switch ($method) {
    case '1':
      return t('DineroMail funds');

    // This code means different methods in each country.
    case '2':
      $methods = array(
        '1' => t('Cash payment in PAGOFACIL, RAPIPAGO, COBROEXPRESS or BAPROPAGOS'),
        '2' => t('Cash through Bank Ticket'),
        '3' => t('Servipag'),
        '4' => t('Cash payment in BANCOMER, BANAMEX, SANTANDER, BANCO DEL BAJIO, IXE BANCO or SCOTIABANK/INVERLAT'),
      );
      return $methods[$country];

    case '3':
      return t('Credit card');

    case '4':
      return t('Bank transfer');

    // Method cod 5 is only valid for Mexico.
    case '5':
      if ($country == '4') {
        return t('Cash in OXXO / 7ELEVEN stores');
      }
      watchdog('uc_dineromail', 'Invalid payment method reported by IPN: !method', array('!method' => $method), WATCHDOG_NOTICE);
      return t('Method 5 only valid for Mexico');

    default:
      watchdog('uc_dineromail', 'Unknown payment method reported by IPN: !method', array('!method' => $method), WATCHDOG_NOTICE);
      return t('Unknown method: !code', array('!code' => $method));
  }
}

/**
 * Returns the IPN URL for the country configured in the settings.
 */
function _uc_dineromail_get_ipn_url() {
  $country = variable_get('uc_dineromail_country', '');
  $countries = array(
    '1' => 'argentina',
    '2' => 'brasil',
    '3' => 'chile',
    '4' => 'mexico',
  );
  if (!empty($country)) {
    return 'http://' . $countries[$country] . '.dineromail.com/Vender/Consulta_IPN.asp';
  }
  return FALSE;
}

/**
 * Logs a notification in the database.
 */
function _uc_dineromail_log_ipn($order_id, $status, $txn_id = NULL, $method = NULL, $amount = NULL, $email = NULL) {
  if (is_null($txn_id)) {
    db_insert('uc_payment_dineromail_ipn')
      ->fields(array(
        'order_id' => $order_id,
        'status' => $status,
        'received' => time(),
      ))
      ->execute();
  }
  else {
    db_update('uc_payment_dineromail_ipn')
      ->fields(array(
        'txn_id' => $txn_id,
        'payment_method' => $method,
        'payment_amount' => $amount,
        'status' => $status,
        'payer_email' => $email,
        'received' => time(),
      ))
      ->condition('order_id', $order_id)
      ->execute();
  }
}
